﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Text;

namespace EasyTelnet
{
    /// <summary>
    /// Byte类型扩展
    /// </summary>
    internal static class BypeTypeExtend
    {
        /// <summary>
        /// 扩展的Equals
        /// </summary>
        /// <param name="aByte"></param>
        /// <param name="telnetCommand"></param>
        /// <returns></returns>
        public static bool ExtEquals(this byte aByte, TelnetCommand telnetCommand)
        {
            return aByte == (byte) telnetCommand;
        }

        /// <summary>
        /// 扩展的Equals
        /// </summary>
        /// <param name="aByte"></param>
        /// <param name="telnetOption"></param>
        /// <returns></returns>
        public static bool ExtEquals(this byte aByte, TelnetOption telnetOption)
        {
            return aByte == (byte) telnetOption;
        }

        /// <summary>
        /// 枚举类型的 byte 值
        /// </summary>
        /// <param name="telnetOptionQualifier"></param>
        /// <returns></returns>
        public static byte ByteValue(this TelnetOptionQualifier telnetOptionQualifier)
        {
            return (byte) telnetOptionQualifier;
        }

        /// <summary>
        /// 枚举类型的 byte 值
        /// </summary>
        /// <param name="telnetCommand"></param>
        /// <returns></returns>
        public static byte ByteValue(this TelnetCommand telnetCommand)
        {
            return (byte) telnetCommand;
        }

        /// <summary>
        /// 枚举类型的 byte 值
        /// </summary>
        /// <param name="telnetOption"></param>
        /// <returns></returns>
        public static byte ByteValue(this TelnetOption telnetOption)
        {
            return (byte) telnetOption;
        }

        /// <summary>
        ///  枚举类型的 byte 值
        /// </summary>
        /// <param name="terminalCommand"></param>
        /// <returns></returns>
        public static byte ByteValue(this TerminalCommand terminalCommand)
        {
            return (byte) terminalCommand;
        }

        public static string BytesString(this byte[] bytes)
        {
            var sbuilder = new StringBuilder();
            foreach (var b in bytes)
            {
                sbuilder.Append(b+" ");
            }
            return sbuilder.ToString();
        }
    }
}