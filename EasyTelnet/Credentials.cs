﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace EasyTelnet
{
    /// <summary>
    ///    Telnet登陆凭据
    /// </summary>
    [Serializable]
    public class Credentials
    {
        private string _commandPrompt = "";
        private string _password = "";
        private string _passwordPrompt = "password:";
        private int _promptTimeout = 6000;
        private string _username = "";
        private string _usernamePrompt = "login:";

        /// <summary>
        ///     Default constructor for Credentials.
        /// </summary>
        public Credentials()
        {
        }

        /// <summary>
        ///     Constructor for Credentials with username, password and command prompt parameters.
        /// </summary>
        /// <param name="userName">The username used during login.</param>
        /// <param name="password">The password used during login.</param>
        /// <param name="usreNamePrompt"></param>
        /// <param name="passwordPrompt"></param>
        /// <param name="commandPrompt">The command prompt to look for after login.</param>
        public Credentials(string userName, string password, string usreNamePrompt, string passwordPrompt, string commandPrompt)
        {
            _username = userName;
            _password = password;
            _usernamePrompt = usreNamePrompt;
            _passwordPrompt = passwordPrompt;
            _commandPrompt = commandPrompt;
        }

        /// <summary>
        ///     Defines the username used during login.
        /// </summary>
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        /// <summary>
        ///     Defines the password used during login.
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        /// <summary>
        ///     定义登陆成功后的命令提示符.登陆后应该立即显示
        /// </summary>
        public string CommandPrompt
        {
            get { return _commandPrompt; }
            set { _commandPrompt = value; }
        }

        /// <summary>
        ///     定义了在登陆期间查找的用户名提示
        /// </summary>
        public string UsernamePrompt
        {
            get { return _usernamePrompt; }
            set { _usernamePrompt = value; }
        }

        /// <summary>
        ///     定义了在登陆期间查找的秘密提示
        /// </summary>
        public string PasswordPrompt
        {
            get { return _passwordPrompt; }
            set { _passwordPrompt = value; }
        }

        /// <summary>
        ///     等待读取提示的超时时间 6000ms
        /// </summary>
        public int PromptTimeout
        {
            get { return _promptTimeout; }
            set { _promptTimeout = value; }
        }
    }
}