﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace EasyTelnet
{
    public class TelnetMutualSession : TelnetSessionBase
    {
        /// <summary>
        ///     创建Telnet脚本会话( 默认端口23 超时时间60秒)
        /// </summary>
        /// <param name="host"></param>
        /// <param name="credentials"></param>
        /// <param name="netVirtualTerminal"></param>
        public TelnetMutualSession(string host, Credentials credentials, NetVirtualTerminal netVirtualTerminal)
            : this(host, 23, credentials, netVirtualTerminal)
        {
        }

        /// <summary>
        ///     创建Telnet脚本会话( 超时时间60秒)
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="credentials"></param>
        /// <param name="netVirtualTerminal"></param>
        public TelnetMutualSession(string host, int port, Credentials credentials, NetVirtualTerminal netVirtualTerminal)
            : this(host, port, 60, 60, credentials, netVirtualTerminal)
        {
        }

        /// <summary>
        ///     创建Telnet脚本会话
        /// </summary>
        /// <param name="host">主机</param>
        /// <param name="port">端口</param>
        /// <param name="receiveTimeout">读取超时</param>
        /// <param name="sendTimeout">发送超时</param>
        /// <param name="credentials"></param>
        /// <param name="netVirtualTerminal">网络虚拟终端</param>
        public TelnetMutualSession(string host, int port, int receiveTimeout, int sendTimeout, Credentials credentials, NetVirtualTerminal netVirtualTerminal)
            : base(host, port, receiveTimeout, sendTimeout, credentials, netVirtualTerminal)
        {
        }


        /// <summary>
        ///     开启交互会话
        /// </summary>
        public override void SessionStart()
        {
            base.SessionStart();
            while (true)
            {
                var cmd = Console.ReadLine();
                if (cmd.Equals("quit", StringComparison.OrdinalIgnoreCase))
                {
                    SendLogout(false);
                    return;
                }
                else
                {
                    SendAsync(cmd, true);
                }
            }
        }
    }
}