﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace EasyTelnet
{
    /*
     * The Telnet protocol uses various commands to control the client-server connection. 
     * These commands are transmitted within the data stream. 
     * The commands are distinguished from the data by setting the most significant bit to 1.
     * (Remember that data is transmitted as 7-bits with the eighth bit set to 0) 
     * Commands are always introduced by the Interpret as command (IAC) character.
     */


    /// <summary>
    ///     Telnet 指令定义
    /// </summary>
    internal enum TelnetCommand : byte
    {
        SE = 240,
        NOP = 241,
        DM = 242,
        BRK = 243,
        IP = 244,
        AO = 245,
        AYT = 246,
        EC = 247,
        EL = 248,

        /// <summary>
        ///     可与 SupressGoAhead，Echo 一起使用
        /// </summary>
        GA = 249,
        SB = 250,

        /// <summary>
        ///     立即执行
        /// </summary>
        WILL = 251,

        /// <summary>
        ///     拒绝执行
        /// </summary>
        WONT = 252,

        /// <summary>
        ///     要求对方执行
        /// </summary>
        DO = 253,

        /// <summary>
        ///     要求对方【不要/停止】执行
        /// </summary>
        DONT = 254,

        /// <summary>
        ///     将跟随在此字符后的字符解释为命令
        /// </summary>
        IAC = 255
    }
}