﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Net.Sockets;

namespace EasyTelnet
{
    /// <summary>
    ///     读写远程数据
    /// </summary>
    internal class DataReadWrite : IDisposable
    {
        private readonly string _host;
        private readonly int _port;
        private readonly int _receiveTimeOut;
        private readonly int _sendTimeOut;

        /// <summary>
        ///     客户端
        /// </summary>
        private Socket _socket;

        /// <summary>
        ///     数据可用事件（一旦接收到数据了就会触发这个事件）
        /// </summary>
        public event Action<DataAvailableEventArgments> DataAvailable;

        /// <summary>
        ///     Telnet 协议解析事件
        /// </summary>
        public event Func<DataStream, DataAvailableEventArgments> TelnetProtocolAnalyzer;


        #region 构造函数

        /// <summary>
        ///     传输构造
        /// </summary>
        /// <param name="host">主机IP</param>
        /// <param name="port">Telnet端口</param>
        public DataReadWrite(string host, int port) : this(host, port, 1000, 1000)
        {
        }

        /// <summary>
        ///     传输构造
        /// </summary>
        /// <param name="host">主机IP</param>
        /// <param name="port">Telnet端口</param>
        /// <param name="receiveTimeOut">接收超时</param>
        /// <param name="sendTimeOut">发送超时</param>
        public DataReadWrite(string host, int port, int receiveTimeOut, int sendTimeOut)
        {
            _host = host;
            _port = port;
            _receiveTimeOut = receiveTimeOut;
            _sendTimeOut = sendTimeOut;
        }

        /// <summary>
        ///     析构
        /// </summary>
        ~DataReadWrite()
        {
            Close();
        }

        #endregion


        #region 连接

        /// <summary>
        ///     连接
        /// </summary>
        public bool Connect()
        {
            try
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Connect(_host, _port);
                _socket.ReceiveTimeout = _receiveTimeOut;
                _socket.SendTimeout = _sendTimeOut;
                _socket.NoDelay = true;

                var readState = new DataReadState(_socket);
                _socket.BeginReceive(readState.Buffer, 0, DataReadState.BUFFER_SIZE, SocketFlags.None, ReadCallback, readState);

                return true;
            }
            catch (Exception ex)
            {
                throw new TelnetTransmissionException("Telnet传输初始化异常", ex);
            }
        }

        #endregion


        #region 异步读回调

        /// <summary>
        ///     检查异步结果有效性
        /// </summary>
        /// <param name="asyncResult"></param>
        /// <returns>true 数据有效</returns>
        internal bool ValidateAsyncResult(IAsyncResult asyncResult)
        {
            return (asyncResult != null);
        }

        /// <summary>
        ///     读取流回调
        /// </summary>
        /// <param name="asyncResult"></param>
        private void ReadCallback(IAsyncResult asyncResult)
        {
            if (!ValidateAsyncResult(asyncResult))
            {
                Close();
                Console.WriteLine("异步操作回调验证错误");
                return;
            }
            var state = (DataReadState) asyncResult.AsyncState;
            var client = state.WorkSocket;
            var bytes = client.EndReceive(asyncResult);

            if (bytes <= 0) return;

            //step1.解析协议报文
            if (TelnetProtocolAnalyzer == null) throw new TelnetTransmissionException("未设置Telnet协议解析事件处理程序：TelnetProtocolAnalyzer");

            var dataStream = new DataStream(client, state.Buffer, 0, bytes);
            var buffer = new byte[bytes];
            for (var i = 0; i < bytes; i++)
            {
                buffer[i] = state.Buffer[i];
            }
            //Console.WriteLine("SERVER:{0}", buffer.BytesString() /*Encoding.Default.GetString(state.Buffer,0,bytes)*/);
            var dataAvaliableArgs = TelnetProtocolAnalyzer(dataStream);

            //step2、通知有数据来了
            if (DataAvailable != null && dataAvaliableArgs.HasData) DataAvailable(dataAvaliableArgs);
            try
            {
                //如果服务器终止的话就会报异常
                //step3、重新开始异步读取
                var readState = new DataReadState(client);
                client.BeginReceive(readState.Buffer, 0, DataReadState.BUFFER_SIZE, SocketFlags.None, ReadCallback, readState);
            }
            catch
            {
                Close();
            }
        }

        #endregion


        #region 异步写回调

        public void WriteStreamCallback(IAsyncResult asyncResult)
        {
            var socket = (Socket) asyncResult.AsyncState;
            socket.EndSend(asyncResult);
        }

        #endregion


        #region 异步写

        /// <summary>
        ///     异步发送
        /// </summary>
        /// <param name="content"></param>
        public void SendAsync(byte[] content)
        {
            if (_socket == null) throw new TelnetTransmissionException("发送失败，还没有创建数据链路。请先调用 Connect");
            if (!_socket.Connected) throw new TelnetTransmissionException("发送失败，数据链路关闭。请重新启动客户端。");
            _socket.BeginSend(content, 0, content.Length, SocketFlags.None, WriteStreamCallback, _socket);
        }

        #endregion


        #region 同步写

        /// <summary>
        ///     同步发送
        /// </summary>
        /// <param name="content"></param>
        public void SendSync(byte[] content)
        {
            if (_socket == null) throw new TelnetTransmissionException("发送失败，还没有创建数据链路。请先调用 Connect");
            if (!_socket.Connected) throw new TelnetTransmissionException("发送失败，数据链路关闭。请重新启动客户端。");
            _socket.Send(content, SocketFlags.None);
        }

        #endregion


        #region 关闭 和 释放

        /// <summary>
        ///     执行与释放或重置非托管资源相关的应用程序定义的任务。
        /// </summary>
        public virtual void Dispose(bool closeSocket)
        {
            if (closeSocket) { 
                _socket.Close();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Close()
        {
            if (_socket == null || !_socket.Connected) return;
            _socket.Close(2);
        }

        #endregion
    }

    /// <summary>
    ///     传输异常
    /// </summary>
    [Serializable]
    public class TelnetTransmissionException : ApplicationException
    {
        public TelnetTransmissionException(string msg) : base(msg)
        {
        }

        public TelnetTransmissionException(string msg, Exception innerException) : base(msg, innerException)
        {
        }
    }
}