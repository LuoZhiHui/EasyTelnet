﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace EasyTelnet
{
    //Telnet Options
    //Options give the client and server a common view of the connection. They can be negotiated at any time during the connection by the use of commands. 
    //They are described in separate RFCs. 
    //
    //The following are examples of common options:
    //
    // Decimal code	    Name	                    RFC
    // 3	                    suppress go ahead	    858
    // 5	                    status	                        859
    // 1	                    echo	                            857
    // 6	                    timing mark	                860
    // 24	                    terminal type	            1091
    // 31	                    window size	                1073
    // 32	                    terminal speed	            1079
    // 33	                    remote flow control	    1372
    // 34	                    linemode	                    1184
    // 36	                    environment variables	1408
    // 
    // Either end of a Telnet conversation can locally or remotely enable or disable an option. 
    // The initiator sends a 3-byte command of the form:
    // 
    //   IAC	Type of Operation	Option
    // 
    // The response is of the same form. Operation is one of: 
    // 
    // Description	Decimal Code	Action
    // WILL	            251	Sender wants to do something.
    // WONT	        252	Sender doesn't want to do something.
    // DO	            253	Sender wants the other end to do something.
    // DONT	        254	Sender wants the other not to do something.
    // 
    // 
    // Associated with each of the these commands are various possible responses: 
    // 
    // Sender Sent	    Receiver Responds	                                                                                                                        Implication
    // WILL DO	        The sender would like to use a certain facility if the receiver can handle it.	                                    Option is now in effect.
    // WILL DONT	    Receiver says it cannot support the option.	                                                                                    Option is not in effect.
    // DO WILL	        The sender says it can handle traffic from the sender if the sender wishes to use a certain option.	Option is now in effect.
    // DO WONT	        Receiver says it cannot support the option.	                                                                                    Option is not in effect.
    // WONT DONT    	Option disabled.	                                                                                                                            DONT is only valid response.
    // DONT WONT	    Option disabled.	                                                                                                                            WONT is only valid response.
    // 
    // 
    // For example, if the sender wants the other end to suppress go-ahead, it would send the byte sequence: 
    // 
    // IAC	WILL	Suppress Go Ahead
    // 
    // The final byte of the 3-byte sequence identifies the required action. 
    // 
    // Some option's values need to be communicated after support of the option has been agreed. 
    // This is done using sub-option negotiation. Values are negotiated using value query commands and responses in the following form:
    // 
    // IAC	SB	option code	1	IAC	SE
    // and
    // IAC	SB	option code	0	IAC	SE
    // 
    // 
    // For example, if the client wants to identify the terminal type to the server, the following exchange might take place: 
    // 
    // CLIENT		IAC	WILL	Terminal Type
    // SERVER		IAC	DO	Terminal Type
    // CLIENT		IAC	SB	Terminal Type	1	IAC	SE
    // SERVER		IAC	SB	Terminal Type	0	V	T	2	2	0	IAC	SE
    // 
    // The first exchange establishes that terminal type (option number 24) is handled, the server then enquires of the client what value it wishes to associate with the terminal type. 
    // The sequence SB,24,1 implies sub-option negotiation for option type 24, value required (1). The IAC,SE sequence indicates the end of this request. 
    // The response IAC,SB,24,0,'V'... implies sub-option negotiation for option type 24, value supplied (0), the IAC,SE sequence indicates the end of the response (and the supplied value). 
    // The encoding of the value is specific to the option but a sequence of characters, as shown above, is common. 

    /// <summary>
    ///     Telnet 选项
    /// </summary>
    internal enum TelnetOption : byte
    {
        /// <summary>
        ///     二进制传输
        /// </summary>
        BinaryTransmission = 0,

        /// <summary>
        ///     回显
        /// </summary>
        Echo = 1,

        /// <summary>
        ///     重新连接
        /// </summary>
        Reconnection = 2,

        /// <summary>
        ///     抑制继续进行
        /// </summary>
        SuppressGoAhead = 3,
        ApproxMessageSize = 4,
        Status = 5,
        TimingMark = 6,
        RemoteControlledTransAndEcho = 7,
        OutputLineWidth = 8,
        OutputPageSize = 9,
        OutputCarriageReturnDisposition = 10,
        OutputHorizontalTabStops = 11,
        OutputHorizontalTabDisposition = 12,
        OutputFormfeedDisposition = 13,
        OutputVerticalTabstops = 14,
        OutputVerticalTabDisposition = 15,
        OutputLinefeedDisposition = 16,
        ExtendedAscii = 17,
        Logout = 18,
        ByteMacro = 19,
        DataEntryTerminal = 20,
        Supdup = 21,
        SupdupOutput = 22,
        SendLocation = 23,

        /// <summary>
        ///     终端类别
        /// </summary>
        TerminalType = 24,
        EndOfRecord = 25,
        TacacsUserIdentification = 26,

        /// <summary>
        /// OutputMarking RFC933
        /// </summary>
        OutputMarking = 27,
        TerminalLocationNumber = 28,
        Telnet3270Regime = 29,
        X3Pad = 30,

        /// <summary>
        ///     协商网络虚拟控制台窗口大小
        /// </summary>
        WindowSize = 31,
        TerminalSpeed = 32,
        RemoteFlowControl = 33,

        /// <summary>
        ///     行模式（RFC1184[Borman1990]）
        /// </summary>
        Linemode = 34,
        XDisplayLocation = 35,
        Environment = 36,
        Authentication = 37,
        Encryption = 38,
        NewEnvironment = 39,
        Tn3270E = 40,
        Xauth = 41,
        Charset = 42,
        TelnetRemoteSerialPortRsp = 43,
        ComPortControl = 44,
        TelnetSuppressLocalEcho = 45,
        TelnetStartTls = 46,
        Kermit = 47,
        SendUrl = 48,
        ForwardX = 49,
        //Unassigned=50-137,
        TeloptPragmaLogon = 138,
        TeloptSspiLogon = 139,
        TeloptPragmaHeartbeat = 140,
        //Unassigned	=141-254,
        ExtendedOptionsList = 255,
    }
}