﻿// The MIT License (MIT)
// Copyright (c) 2014 LuoZhihui
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;

namespace EasyTelnet
{
    /// <summary>
    ///     数据可用通知事件 参数
    /// </summary>
    public class DataAvailableEventArgments : EventArgs,IDisposable
    {
        private List<byte> _data = new List<byte>();

        /// <summary>
        ///     数据可用通知事件
        /// </summary>
        public DataAvailableEventArgments()
        {
            _data = new List<byte>();
        }

        ~DataAvailableEventArgments()
        {
            Dispose(false);
        }

        /// <summary>
        ///     有数据
        /// </summary>
        public bool HasData
        {
            get { return _data.Count > 0; }
        }

        /// <summary>
        ///     到达的数据
        /// </summary>
        public List<byte> Data
        {
            get { return _data; }
        }

        /// <summary>
        ///     执行与释放或重置非托管资源相关的应用程序定义的任务。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _data = null;
            }
        }

        /// <summary>
        ///     写入读取结果
        /// </summary>
        /// <param name="byteData"></param>
        internal void WriteByte(byte byteData)
        {
            _data.Add(byteData); 
        }

        internal void Write(byte[] bytesData,int offset,int count)
        {
            for (var i = offset; i < count && i < bytesData.Length; i++) { 
                _data.Add(bytesData[i]);
            }
        }

        internal void Write(params byte[] bytesData)
        {
            foreach (var aByte in bytesData)
            {
                _data.Add(aByte);
            }
        }
    }
}